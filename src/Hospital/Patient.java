/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Hospital;

import java.util.ArrayList;

/**
 *
 * @author grant
 */
public class Patient {
    private String name;
    private int dateOfBirth;
    private String familyDoc;
    public ArrayList <Band> bands = new ArrayList <Band>();
    private String ResearchGroup;

    public Patient(String name, int dateOfBirth, String familyDoc) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.familyDoc = familyDoc;
        
    }

   

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(int dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getFamilyDoc() {
        return familyDoc;
    }

    public void setFamilyDoc(String familyDoc) {
        this.familyDoc = familyDoc;
    }

  
  
    public ArrayList<Band> getBands() {
        return bands;
    }

    public void setBands(ArrayList<Band> bands) {
        this.bands = bands;
    }

    public String getResearchGroup() {
        return ResearchGroup;
    }

    public void setResearchGroup(String ResearchGroup) {
        this.ResearchGroup = ResearchGroup;
    }
    
     public void addBand(Band b){
        bands.add(0,b);
    } 
    public void removeBand(Band band){
        this.bands.remove(band);
    }
   
}
