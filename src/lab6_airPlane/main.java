/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab6_airPlane;
//
/**
 *
 * @author grant
 */
public class main {
    
    public static void main (String [] args){
        Airport current = new Airport("Tor","Toronto");
        Airport destination = new Airport("LA","Los Angeles");
        Airline airCanada = new Airline("AirCanada","Canadas premiere airline");
        Airplane bigPlane = new Airplane(101,"BP101", 500, "BigPlane", airCanada);
        Flight flight = new Flight(airCanada,bigPlane, 10012, 10122022,current, destination);
    }
    
}
